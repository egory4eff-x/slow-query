#FROM golang:1.19-alpine AS builder
#COPY . /go/src/gitlab.com/egory4eff-x/slow-query
#WORKDIR /go/src/gitlab.com/egory4eff-x/slow-query
## Create slimest possible image
#RUN go build -ldflags="-w -s" -o /go/bin/server /go/src/gitlab.com/egory4eff-x/slow-query/cmd/api
#
#FROM alpine:3.13
## Copy binary from builder
#COPY --from=builder /go/bin/server /go/bin/server
#COPY ./public /app/public
#COPY ./.env /app/.env
#
#WORKDIR /app
## Set entrypoint
#ENTRYPOINT ["/go/bin/server"]

#Build stage
FROM golang:1.19-alpine AS builder
WORKDIR /app
#Copy everything in the slow-log to the working directory in the docker
COPY slow-log .
#Build the file which has the name gateway
RUN go build -o slow-log ./cmd/api/main.go

#Run stage
FROM alpine:3.13
WORKDIR /app
COPY --from=builder  /app/slow-log .
#Which port container listens to
#EXPOSE 8080
COPY public /app/public
COPY .env /app/.env
COPY ./pg_logs /app/pg_logs
#The path to the executable file in the container
CMD ["/app/slow-log"]