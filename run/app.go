package run

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	db2 "gitlab.com/egory4eff-x/slow-query/db"
	"gitlab.com/egory4eff-x/slow-query/geo"
	cservice "gitlab.com/egory4eff-x/slow-query/module/courier/service"
	cstorage "gitlab.com/egory4eff-x/slow-query/module/courier/storage"
	"gitlab.com/egory4eff-x/slow-query/module/courierfacade/controller"
	cfservice "gitlab.com/egory4eff-x/slow-query/module/courierfacade/service"
	oservice "gitlab.com/egory4eff-x/slow-query/module/order/service"
	ostorage "gitlab.com/egory4eff-x/slow-query/module/order/storage"
	"gitlab.com/egory4eff-x/slow-query/router"
	"gitlab.com/egory4eff-x/slow-query/server"
	"gitlab.com/egory4eff-x/slow-query/workers/order"
	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	// получение хоста и порта redis
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPwd := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	// инициализация клиента redis
	db, err := db2.NewPostgresDB(dbHost, dbPort, dbUser, dbPwd, dbName)
	if err != nil {
		return err
	}

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(db)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(db)
	// инициализация сервиса курьеров
	courierService := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierService, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	prometheus.MustRegister(controller.CourierGetStatus, controller.MoveCourier, ostorage.RepoStatus)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
