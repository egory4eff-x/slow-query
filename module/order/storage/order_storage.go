package storage

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/egory4eff-x/slow-query/module/order/models"
	"time"
)

var (
	RepoStatus = prometheus.NewHistogramVec(prometheus.HistogramOpts{Name: "RepoStatus"},
		[]string{"repository", "method"})
)

type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // получить заказ по id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	storage *sqlx.DB
}

func NewOrderStorage(storage *sqlx.DB) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	start := time.Now()
	_, err := o.storage.ExecContext(ctx, `INSERT INTO orders (price, delivery_price, lng, lat, location, is_delivered, created_at) VALUES ($1, $2, $3, $4,st_makepoint($3, $4), $5, $6)`, order.Price, order.DeliveryPrice, order.Lng, order.Lat, order.IsDelivered, order.CreatedAt)
	if err != nil {
		return err
	}
	since := time.Since(start)
	RepoStatus.WithLabelValues("order_storage", "save").Observe(float64(since))
	return nil
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	result := time.Now().Add(-maxAge).Format("2006-01-02 15:04:05")
	stmt, err := o.storage.PreparexContext(ctx, `DELETE FROM orders WHERE created_at <= $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, result)
	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	order := &models.Order{}
	err := o.storage.GetContext(ctx, &order, `SELECT * FROM orders WHERE id = $1`, orderID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return order, nil
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	var count int
	err := o.storage.GetContext(ctx, &count, `SELECT COUNT(*) FROM orders`)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var result []models.Order
	err := o.storage.SelectContext(ctx, &result, fmt.Sprintf(`
			SELECT id, price, delivery_price, lng, lat, is_delivered, created_at
			FROM orders
			WHERE st_distancesphere(location, st_makepoint(%f, %f)) <= %f;
	`, lng, lat, radius))
	if err != nil {
		return nil, fmt.Errorf("get by radius err: %v", err)
	}

	return result, nil
}
