package service

import (
	"context"
	"gitlab.com/egory4eff-x/slow-query/module/order/models"
	"gitlab.com/egory4eff-x/slow-query/module/order/service/mocks"
	"testing"
	"time"
)

func TestOrderService_GetByRadius(t *testing.T) {
	type args struct {
		ctx    context.Context
		lng    float64
		lat    float64
		radius float64
		unit   string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "First mock",
			args: args{
				ctx:    context.Background(),
				lng:    1,
				lat:    2,
				radius: 500,
				unit:   "m",
			},
			wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			orderService := mocks.NewOrderer(t) // made a mock

			orderService.On("GetByRadius", tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit).Return([]models.Order{}, nil)

			o := orderService
			_, err := o.GetByRadius(tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByRadius() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestOrderService_GetCount(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Second mock",
			args: args{ctx: context.Background()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			orderService := mocks.NewOrderer(t)

			orderService.On("GetCount", tt.args.ctx).Return(0, nil)

			o := orderService
			_, err := o.GetCount(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestOrderService_RemoveOldOrders(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Third mock",
			args: args{ctx: context.Background()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			orderService := mocks.NewOrderer(t)

			orderService.On("RemoveOldOrders", tt.args.ctx).Return(nil)

			o := orderService

			if err := o.RemoveOldOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("RemoveOldOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_Save(t *testing.T) {
	type args struct {
		ctx   context.Context
		order models.Order
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Fourth mock",
			args: args{
				ctx: context.Background(),
				order: models.Order{
					ID:            1,
					Price:         12,
					DeliveryPrice: 1234,
					Lng:           1,
					Lat:           3,
					IsDelivered:   false,
					CreatedAt:     time.Now(),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			orderService := mocks.NewOrderer(t)

			orderService.On("Save", tt.args.ctx, tt.args.order).Return(nil)

			o := orderService
			if err := o.Save(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
