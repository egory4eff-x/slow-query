package storage

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/egory4eff-x/slow-query/module/courier/models"
	"gitlab.com/egory4eff-x/slow-query/module/order/storage"
	"time"
)

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *sqlx.DB
}

func NewCourierStorage(storage *sqlx.DB) *CourierStorage {
	return &CourierStorage{storage: storage}
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	start := time.Now()
	_, err := c.storage.ExecContext(ctx,
		`INSERT INTO couriers (score, location) VALUES ($1, st_makepoint($2,$3)) ON CONFLICT (score) DO UPDATE SET location = EXCLUDED.location`, courier.Score, courier.Location.Lng, courier.Location.Lat)
	if err != nil {
		return err
	}
	since := time.Since(start)
	storage.RepoStatus.WithLabelValues("courier_storage", "save").Observe(float64(since))
	return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	var courier models.Courier

	err := c.storage.GetContext(ctx, &courier.Location, `SELECT st_x(st_centroid(st_transform(location, 4326))) AS lng,
       st_y(st_centroid(st_transform(location, 4326))) AS lat FROM couriers WHERE score = 0`)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &courier, err
}
