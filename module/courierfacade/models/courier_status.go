package models

import (
	cm "gitlab.com/egory4eff-x/slow-query/module/courier/models"
	om "gitlab.com/egory4eff-x/slow-query/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
