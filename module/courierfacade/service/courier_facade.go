package service

import (
	"context"
	cservice "gitlab.com/egory4eff-x/slow-query/module/courier/service"
	cfm "gitlab.com/egory4eff-x/slow-query/module/courierfacade/models"
	oservice "gitlab.com/egory4eff-x/slow-query/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius = 2800 // 2.8km
)

//go:generate mockery --name=CourierFacer
type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Println(err)
		return
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {

	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return cfm.CourierStatus{}
	}
	orders, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "m")

	if err != nil {
		log.Println("ERR WHILE GETTING BY RADIUS")
		return cfm.CourierStatus{}
	}

	courierStatus := &cfm.CourierStatus{
		Courier: *courier,
		Orders:  orders,
	}

	return *courierStatus
}
