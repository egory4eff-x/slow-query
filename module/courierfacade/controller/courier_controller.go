package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/egory4eff-x/slow-query/module/courierfacade/service"
	"log"
	"time"
)

var (
	MoveCourier = prometheus.NewHistogram(prometheus.HistogramOpts{Name: "CourierMove"})

	CourierGetStatus = prometheus.NewHistogram(prometheus.HistogramOpts{Name: "CourierGetStatus"})
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(time.Millisecond * 50)

	start := time.Now()
	courierStatus := c.courierService.GetStatus(ctx)
	// получить статус курьера из сервиса courierService используя метод GetStatus
	// отправить статус курьера в ответ
	ctx.JSON(200, courierStatus)
	since := time.Since(start)
	CourierGetStatus.Observe(float64(since))
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	mData, ok := m.Data.([]byte)
	if !ok {
		log.Fatal(err)
	}
	err = json.Unmarshal(mData, &cm)
	if err != nil {
		log.Fatal(err)
	}
	start := time.Now()
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
	since := time.Since(start)

	MoveCourier.Observe(float64(since))
}
